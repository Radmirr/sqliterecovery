package sqliterecovery.main.extensions

import java.io.RandomAccessFile

fun RandomAccessFile.read(length: Int): ByteArray {
    val buffer = Array<Byte>(length, { 0 }).toByteArray()
    read(buffer)
    return buffer
}