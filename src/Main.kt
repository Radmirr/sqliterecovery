package sqliterecovery.main

import sqliterecovery.main.csv.CSVWriter
import sqliterecovery.main.gui.SrApp
import sqliterecovery.main.sqlite.FreeBlock
import sqliterecovery.main.sqlite.Recover
import tornadofx.launch

class Main {

    companion object {

        private val testPath1 = "C:\\RegistryTest\\test1.db"
        private val testPath2 = "C:\\RegistryTest\\test2.db"
        private val browserHistory = "C:\\Users\\Radmir\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\History"
        private val operaHistory = "C:\\Users\\Radmir\\AppData\\Roaming\\Opera Software\\Opera Stable\\History"

        @JvmStatic
        fun main(args: Array<String>) {
            launch<SrApp>()
        }
    }
}