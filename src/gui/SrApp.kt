package sqliterecovery.main.gui

import javafx.scene.control.TextField
import javafx.stage.FileChooser
import sqliterecovery.main.csv.CSVWriter
import sqliterecovery.main.sqlite.FreeBlock
import sqliterecovery.main.sqlite.Recover
import tornadofx.*
import java.io.File

class SrApp : App(SrView::class, InternalWindow.Styles::class) {
}

class SrView : View("SQLite Recovery Tool") {
    private val ef = arrayOf(
        FileChooser.ExtensionFilter("SQLite db files (*.db)", "*.db"),
        FileChooser.ExtensionFilter("All files", "*")
    )

    private lateinit var tfDbSource: TextField
    private lateinit var tfRecoverDir: TextField

    override val root = vbox(10) {
        padding = insets(10)
        minWidth = 300.0

        vbox {
            spacing = 4.0

            label("Выберите файл SQLite")
            borderpane {
                center = vbox {
                    padding = insets(0, 10, 0, 0)
                    tfDbSource = textfield()
                }
                right = vbox {
                    button("...") {
                        action {
                            selectFile(tfDbSource)
                        }
                    }
                }
            }
        }

        vbox {
            spacing = 4.0

            label("Результат")
            borderpane {
                center = vbox {
                    padding = insets(0, 10, 0, 0)
                    tfRecoverDir = textfield()
                }
                right = vbox {
                    button("...") {
                        action {
                            selectDir(tfRecoverDir)
                        }
                    }
                }
            }
        }

        button("Начать исследование файла") {
            useMaxWidth = true
            action { makeRecover() }
        }
    }

    private fun selectFile(tf: TextField) {
        val files = chooseFile("Chooose .db file", ef, FileChooserMode.Single)

        if (files.size == 1) {
            tf.text = files[0].path
        }
    }

    private fun selectDir(tf: TextField) {
        val dir = chooseDirectory { }

        if (dir != null) {
            tf.text = dir.path
        }
    }

    private fun makeRecover() {
        val sourceFile = File(tfDbSource.text)
        val resultDir = tfRecoverDir.text

        try {
            if (sourceFile.exists()) {
                val outputFile = File(resultDir, sourceFile.name + ".csv")
                val recover = Recover()
                val result = recover.getFreeBlocks(sourceFile.absolutePath)
                val table = getTable(result)
                CSVWriter().writeToCSV(outputFile.absolutePath, table)

                information(
                    title = "Сообщение",
                    header = "Завершено",
                    content = "Исследование завершено"
                )
            }
        } catch (e: Exception) {
            error(
                title = "Ошибка",
                header = "Ошибка при исследование файла",
                content = e.message
            )
        }
    }

    private fun getTable(data: List<FreeBlock>): Array<Array<String?>> {
        val header = arrayListOf(arrayOf<String?>("Offset", "Size", "Data"))
        data.forEach {
            header.add(
                arrayOf(
                    it.offset.toString(),
                    it.data.size.toString(),
                    it.dataString()
                )
            )
        }
        return header.toTypedArray()
    }
}