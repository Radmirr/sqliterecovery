package sqliterecovery.main.csv

import java.io.PrintWriter
import java.text.SimpleDateFormat
import java.util.*

class CSVWriter {

    private val separator = ";"

    fun writeToCSV(fileName: String, data: Array<Array<String?>>){
        val filename = getFilename(fileName)
        val fileWriter = PrintWriter(filename, "cp1251")
        data.forEach {
            val line = it.joinToString(separator)
            fileWriter.write(line + "\n")
        }
        fileWriter.close()
    }

    private fun getFilename(fileName: String) : String {
        val formater = SimpleDateFormat("dd_MM_yyyy_HH_mm_ss", Locale.getDefault())
        return "$fileName ${formater.format(Date())}.csv"
    }
}