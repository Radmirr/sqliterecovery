package sqliterecovery.main.sqlite

import sqlite.Struct
import sqliterecovery.main.extensions.read
import java.io.RandomAccessFile

class Recover {

    fun getFreeBlocks(filename: String): List<FreeBlock> {
        val result = mutableListOf<FreeBlock>()
        val struct = Struct()
        val file = RandomAccessFile(filename, "r")

        file.seek(0)

        val header =  file.read(16)

        if (!String(header).contains("SQLite")) {
            throw NotSQLiteException()
        }
        val fileSize = file.length()
        val pageSize = struct.unpack(">h", file.read(2))[0]

        var offset = 0L

        while (offset < fileSize) {
            file.seek(offset)
            val flag = struct.unpack(">b", file.read(1))[0]

            if (flag == 13L) {
                var freeblockOffset = struct.unpack(">h", file.read(2))[0]
                val numCells = struct.unpack(">h", file.read(2))[0].toInt()
                val cellOffset =struct.unpack(">h", file.read(2))[0].toInt()
                val numFreeBytes =struct.unpack(">b", file.read(1))[0].toInt()

                val start = 8 + (numCells * 2)
                val length = cellOffset - start
                file.read(numCells * 2)
                val unallocated = file.read(length)
                val a = String(unallocated)

                while (freeblockOffset != 0L) {
                    file.seek(offset + freeblockOffset)
                    val nextFBOffset = struct.unpack(">h", file.read(2))[0]
                    val freeBlockSize = struct.unpack(">h", file.read(2))[0]

                    file.seek(offset + freeblockOffset)
                    val freeBlockData = file.read(freeBlockSize.toInt())
                    val freeBlock = FreeBlock(offset + freeblockOffset, freeBlockData)

                    result.add(freeBlock)

                    freeblockOffset = nextFBOffset
                }
            }

            offset += pageSize
        }

        return result
    }
}

class NotSQLiteException: Exception()

