package sqliterecovery.main.sqlite

class FreeBlock(
    val offset: Long,
    val data: ByteArray
) {

    fun dataString(): String {
        return String(data).replace("\n", "")
            .replace("\r", "")
    }
}